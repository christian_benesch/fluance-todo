import { SwissTodoPage } from './app.po';

describe('swiss-todo App', function() {
  let page: SwissTodoPage;

  beforeEach(() => {
    page = new SwissTodoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
