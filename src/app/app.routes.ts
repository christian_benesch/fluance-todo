import  { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskEditComponent } from './task-edit/task-edit.component'

const routes: Routes = [
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'todo/:index/edit', component: TaskEditComponent
    },
    {
        path: 'todo/:id', component: TaskDetailsComponent,
    }
];

export const appRoutes =
    RouterModule.forRoot(routes);