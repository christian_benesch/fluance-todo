import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { TopTaskComponent } from './top-task/top-task.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskComponent } from './task/task.component';
import { CompleteFilterPipe } from './complete-filter.pipe';
import { TaskService } from "./task.service";
import { ReversePipe } from './reverse.pipe';
import { AboutComponent } from './about/about.component';

import { appRoutes } from './app.routes';
import {ModalModule} from "ng2-modal";
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskEditComponent } from './task-edit/task-edit.component';


@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    TopTaskComponent,
    TaskListComponent,
    TaskComponent,
    CompleteFilterPipe,
    ReversePipe,
    AboutComponent,
    TaskDetailsComponent,
    TaskEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    appRoutes,
    ModalModule
  ],
  providers: [ TaskService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
