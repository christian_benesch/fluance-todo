import { Component, NgZone } from '@angular/core';
import { TaskDetails } from './task-details/task-details.component'
import { Task } from './task.service'
import { TaskService } from './task.service'
import { AppHeaderComponent } from './app-header/app-header.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ TaskService ],
})
export class AppComponent {

  title = 'To Do!';
  tasks: Task[];

  constructor( private taskService: TaskService) {
    this.tasks = taskService.getTasks()
  }

  toDoCount(){
    return this.taskService.toDoCount();
  }

  doneCount(){
    return this.taskService.doneCount();
  }

  updateLists(){
    console.log("APP: updating Lists")
    this.tasks = this.taskService.refresh();
  }

  createTask(task:Object){
    this.taskService.create(task.title, task.note);
    console.log(`APP:Task ${task.title} created`);
    this.updateLists();

  }
  deleteTask(task:Task){
    console.log(`APP:Task ${task.index} to be deleted`);
    this.taskService.delete(task.index);
    this.updateLists();
  }
  onTaskCompleted() {
    console.log("APP: task completed");
  }
}
