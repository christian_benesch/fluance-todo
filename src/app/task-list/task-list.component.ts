import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../task.service'
import { TaskService } from '../task.service'

@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
  providers: [ TaskService ]
})
export class TaskListComponent implements OnInit {
  @Input('tasks')  tasks:Task[];
  @Input('completed') completed:boolean;
  // completed - filter var to distinguish
  // between todo list and done list.

  @Output() onTaskCompleted = new EventEmitter<Task[]>();
  @Output() onTaskDeleted = new EventEmitter<Task>();


  constructor(private taskService: TaskService) {

  }

  onCompleted() {
    console.log("TASKLIST: task completed");
    this.onTaskCompleted.emit(this.tasks);
  }

  onDeleted(task:Task){
    console.log(`TASKLIST: task ${task.index} to be deleted`);
    this.onTaskDeleted.emit(task);
  }

  ngOnInit() {
    this.tasks = this.taskService.getTasks();
  }

}
