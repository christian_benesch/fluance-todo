import { Injectable } from '@angular/core';


export class Task {

  constructor(
    public index: number,
    public title:string,
    public note: string,
    public completed: boolean){


  }
  complete(){
    this.completed=true;
  }

  toggleComplete(){
    this.completed = !this.completed;
  }

  uncomplete(){
    this.completed=false;
  }
}


@Injectable()
export class TaskService {

  private tasks: Task[];
  private autoincrement:number;


  constructor() {
    this.autoincrement = 0;
    this.tasks = [];

    this.create('shopping groceries','tea, biscuits, flower, eggs, salad');
    this.create('hoover flat','replace bag');
    this.create('buy subscription','note');
    this.create('finish fluance prototype','improve layout');
    this.create('learn angular 2','book: Angular JS 2 Intro');
  }

  public getTasks(): Task[] {
    return this.tasks;
  }

  public getToDoTasks(): Task[]{
    var res:Task[]=[];
    for (let task of this.tasks){
      if(!task.completed){
        res.push(task)
      }
    }
    return res;
  }

  public getDoneTasks(): Task[]{
    var res:Task[]=[];
    for (let task of this.tasks){
      if(task.completed){
        res.push(task)
      }
    }
    return res;
  }

  public refresh(): Task[]{
    // slicing forces a new array reference
    // this makes sure that the ngFor loops refresh their
    // sub-components
    return this.tasks.slice();
  }

  public toDoCount(){
    return this.getToDoTasks().length;
  }

  public doneCount(){
    return this.getDoneTasks().length;
  }



  public create(title: string , note: string){
      console.log(`SRV:creating ${title} `)
      this.autoincrement++;
      var newTask = new Task(
                this.autoincrement,
                title,
                note,
                false);
      this.tasks.push(newTask);
      this.dump();
  }

  public dump(){
    for(let task of this.tasks){
      console.log(`[Task index='${task.index}' title='${task.title}' note='${task.note}']`);
    }
}

  public delete(index:number){
      var res:Task[] = [];
      for(let task of this.tasks){
        if(task.index==index)
          continue;
        else
          res.push(task);
      }
      this.tasks=res;
      console.log(`SERVICE: task ${index} deleted`);
      this.dump();

  }
}
