import { Pipe, PipeTransform } from '@angular/core';
import { Task } from './task.service'

@Pipe({
  name: 'completeFilter'
})
export class CompleteFilterPipe implements PipeTransform {
  // filters the tasks list according to the supplied
  // 'completed' argument
  // this distinguishes todo and done list.
  transform(tasks: Task[] , completed: boolean): any {
    var res: Task[] = [];
    for (let task of tasks){
      if(completed==task.completed){
        res.push(task)
      }
    }
    return res;
  }

}
