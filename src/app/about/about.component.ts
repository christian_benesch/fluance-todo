import { Component, OnInit } from '@angular/core';
// TODO: Resolve JQUERY import issue
declare var $:JQueryStatic;

interface JQuery {
  chosen(options?:any):JQuery;
}
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      $('.ui.modal').modal('show');
  }
}
