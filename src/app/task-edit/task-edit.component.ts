import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute, Params, NavigationStart } from "@angular/router"
import 'rxjs/add/operator/pairwise';


@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  public taskIndex: number;

  constructor(private route: ActivatedRoute,
              private router: Router,) {
}


  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params['index']){
        this.taskIndex=params['index'];
    }
  });
  }

}
