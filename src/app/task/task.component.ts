import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from "../task.service"
import { Router,ActivatedRoute, Params } from "@angular/router"

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input('task') task:Task;
  @Output() onCompleted = new EventEmitter<boolean>();
  @Output() onDeleted = new EventEmitter<Task>();

  taskIndex: number;

  constructor(    private route: ActivatedRoute,
                  private router: Router,
  ) { }

  toggleComplete(){
    this.task.toggleComplete();
    this.onCompleted.emit(true);
  }

  deleteTask(task:Task){
    console.log(`TASK: Task ${task.index} to be deleted`)
    this.onDeleted.emit(task);
  }

  editTask(task: Task) {
    var url = this.router.createUrlTree(['/todo', task.index, 'edit']);
    console.log(url);
    this.router.navigateByUrl(url, true);
  }


  ngOnInit() {

  }

}
