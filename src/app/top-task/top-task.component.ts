import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Task, TaskService } from '../task.service'



@Component({
  selector: 'top-task',
  templateUrl: './top-task.component.html',
  styleUrls: ['./top-task.component.css'],
  providers: [ TaskService ]
})
export class TopTaskComponent implements OnInit {
  @Output() onTaskCreated = new EventEmitter<Object>();
  constructor(private taskService:TaskService) {}

  onEnter(value:string){
    // we are not creating new tasks if enter is
    // pressed on an empty string
    if(value.length === 0 || !value.trim())
      return;

    console.log(`Task '${value}' to be created`);
    // this.taskService.create(value,'note');
    this.onTaskCreated.emit({title: value, note:'note'});
  }
  ngOnInit() {
  }

}
